package com.evgenijpopov.loginapp;

import com.evgenijpopov.loginapp.Utils.Encryption;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EncryptionTest {
    private Encryption encryption;
    private final int STEPS = 10;

    @Before
    public void setUp() throws Exception {
        encryption = new Encryption();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void saltIsUniq() throws Exception {
        Encryption encryption1;
        for (int i = 0; i < STEPS; i++) {
            encryption1 = new Encryption();
            assertNotEquals(encryption.getSalt(), encryption1.getSalt());
        }
    }

    @Test
    public void testHashing() throws Exception {
        String pass, hashPass;
        for (int i = 0; i < STEPS; i++) {
            encryption = new Encryption();
            pass = encryption.getSalt();
            hashPass = encryption.hashPassword(pass);
            System.out.printf("pass: %s, hash: %s\n", pass, hashPass);
            assertTrue(encryption.verifyPassword(hashPass, pass));
        }
    }

    @Test
    public void someTest() throws Exception {
        String pass, hashPass;
        for (int i = 0; i < STEPS; i++) {
            encryption = new Encryption();
            pass = encryption.getSalt();
            hashPass = encryption.hashPassword(pass);
            System.out.printf("pass: %s, hash: %s\n", pass, hashPass);
            assertTrue(encryption.verifyPassword(hashPass, pass));
        }
    }


}
