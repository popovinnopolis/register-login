package com.evgenijpopov.loginapp;

import com.evgenijpopov.loginapp.Managers.ManagerGeneric;
import com.evgenijpopov.loginapp.Managers.ManagerGroup;
import com.evgenijpopov.loginapp.Managers.ManagerUser;
import com.evgenijpopov.loginapp.Models.Contact;
import com.evgenijpopov.loginapp.Models.ContactType;
import com.evgenijpopov.loginapp.Models.Group;
import com.evgenijpopov.loginapp.Models.Person;
import com.evgenijpopov.loginapp.Models.Student;
import com.evgenijpopov.loginapp.Models.Teacher;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;


public class ManagersTest {
    Person student, teacher;
    ManagerGeneric<Student> studentManagerGeneric;
    Group group;

    @Before
    public void setUp() throws Exception {
        group = new Group("STC_06", null);
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("email@email.ru", ContactType.EMAIL));
        contacts.add(new Contact("email1@email.ru", ContactType.EMAIL));
        student = new Student("John", "Connor", "Reese", new Date(), group, contacts);
        teacher = new Teacher("Fred", "Kruger", "Charles", new Date(), null);

        ManagerUser.getInstance().create(student);
        ManagerUser.getInstance().create(teacher);
    }

    @Test
    public void find() throws Exception {
        Map<String, Object> condition = new HashMap<>();
        condition.put("firstName", "Fred");
        condition.put("secondName", "Charles");
        condition.put("group", group);

        Student stTest = studentManagerGeneric.findFirst(condition);
        assertTrue(teacher.equals(stTest));

        condition = new HashMap<>();
        condition.put("firstName", "Fredy");
        condition.put("secondName", "Charles");
        condition.put("group", group);

        stTest = studentManagerGeneric.findFirst(condition);
        assertFalse(teacher.equals(stTest));

    }

    @Test
    public void testSingletonGroups() throws Exception {
        ManagerGroup managerGroup = ManagerGroup.getInstance();
        managerGroup.create(group);

        Map<String, Object> condition = new HashMap<>();
        condition.put("name", "STC_06");

        assertEquals(managerGroup.findFirst(condition), group);

        condition.put("name", "STC_061");
        assertNotEquals(managerGroup.findFirst(condition), group);
    }

    @Test
    public void testSingletonStudents() throws Exception {
        ManagerUser manager = ManagerUser.getInstance();

        Map<String, Object> condition = new HashMap<>();
        condition.put("firstName", "Fred");
        condition.put("secondName", "Charles");
        condition.put("group", group);

        Person stTest = manager.findFirst(condition);
        assertFalse(teacher.equals(stTest));

        condition = new HashMap<>();
        condition.put("firstName", "John");
        condition.put("secondName", "Reese");
        condition.put("group", group);

        assertTrue(student.equals(manager.findFirst(condition)));
    }
}
