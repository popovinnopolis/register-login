package com.evgenijpopov.loginapp.Models;

import com.evgenijpopov.loginapp.Utils.IdGenerator;

import java.io.Serializable;

public abstract class Entity implements Serializable {
    transient private static final long serialVersionUID = -5033508417639375633L;
    protected long id;

    public Entity() {
        this.id = IdGenerator.getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        if (!(obj instanceof Entity)) return false;
        return this.id ==  ((Entity) obj).getId();
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 42);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String toString() {
        return String.format("%d", id);
    }
}
