package com.evgenijpopov.loginapp.Models;


import java.util.Date;
import java.util.List;

public class User extends Person {
    public User(String firstName, String surnameName, String secondName, Date dateOfBirth,
                List<Contact> contacts, Role role, String password) {
        super(firstName, surnameName, secondName, dateOfBirth, contacts, role, password);
    }
}
