package com.evgenijpopov.loginapp.Models;

import java.util.ArrayList;
import java.util.List;

public class Group extends Entity {
    transient private static final long serialVersionUID = -3994677916602304047L;
    private String name;
    private List<Student> students;

    public Group(String name, List<Student> students) {
        super();
        this.name = name;
        this.students = students == null ? new ArrayList<Student>() : students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return String.format("%d, name: %s (%d)", id, name, students.size());
    }
}
