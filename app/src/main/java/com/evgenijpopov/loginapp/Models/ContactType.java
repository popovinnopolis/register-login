package com.evgenijpopov.loginapp.Models;

public enum ContactType {
    PHONE,
    EMAIL,
    TELEGRAM,
    SKYPE,
    VK,
    FACEBOOK,
    LINKEDIN,
    ADDRESS
}
