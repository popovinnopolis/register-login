package com.evgenijpopov.loginapp.Models;

public enum Role {
    ADMIN,
    STUDENT,
    TEACHER,
    GUEST
}
