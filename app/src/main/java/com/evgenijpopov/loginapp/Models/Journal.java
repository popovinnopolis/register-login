package com.evgenijpopov.loginapp.Models;

public class Journal extends Entity {
    transient private static final long serialVersionUID = -1288409580042205715L;
    private Person student;
    private Lesson lesson;
    private boolean present; // Присутствовал ли студент?

    public Journal(Person student, Lesson lesson, boolean present) {
        super();
        this.student = student;
        this.lesson = lesson;
        this.present = present;
    }

    public Person getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public boolean isPresent() {
        return present;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", student, lesson, present);
    }

}