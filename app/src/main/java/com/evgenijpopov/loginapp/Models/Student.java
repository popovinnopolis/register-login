package com.evgenijpopov.loginapp.Models;

import java.util.Date;
import java.util.List;

public class Student extends Person {
    transient private static final long serialVersionUID = 1809430313261280407L;
    private Group group;

    public Student(String firstName, String surnameName, String secondName, Date dateOfBirth,
                   Group group, List<Contact> contacts) {
        super(firstName, surnameName, secondName, dateOfBirth, contacts, Role.STUDENT);
        this.group = group;
        this.group.getStudents().add(this);
    }

    public long getGroupId() {
        return group.getId();
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

}
