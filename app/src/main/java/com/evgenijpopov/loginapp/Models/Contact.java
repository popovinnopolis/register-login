package com.evgenijpopov.loginapp.Models;

public class Contact extends Entity  {
    transient private static final long serialVersionUID = -6646732800687978080L;
    private String value;
    private ContactType type;

    public Contact(String value, ContactType type) {
        super();
        this.value = value;
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }
}
