package com.evgenijpopov.loginapp.Models;

import java.util.Date;
import java.util.List;

public class Teacher extends Person {
    public Teacher(String firstName, String surnameName, String secondName, Date dateOfBirth,
                   List<Contact> contacts, String password) {
        super(firstName, surnameName, secondName, dateOfBirth, contacts, Role.TEACHER, password);
    }

    public Teacher(String firstName, String surnameName, String secondName, Date dateOfBirth,
                   List<Contact> contacts) {
        this(firstName, surnameName, secondName, dateOfBirth, contacts, null);
    }
}
