package com.evgenijpopov.loginapp.Models;

import java.util.Date;
import java.util.List;

public class Lesson extends Entity {
    transient private static final long serialVersionUID = 5120421978462054804L;
    private String name;
    private Date timeFrom;
    private Date timeTo;
    private String auditory;
    private String description;
    private String subject;
    private Person lector;
    private List<Long> groupId;

    public Lesson(String name, Date timeFrom, Date timeTo, String auditory, String description, String subject,
                  Person lector, List<Long> groupId) {
        super();
        this.name = name;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.auditory = auditory;
        this.description = description;
        this.subject = subject;
        this.lector = lector;
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(Date timeFrom) {
        this.timeFrom = timeFrom;
    }

    public Date getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(Date timeTo) {
        this.timeTo = timeTo;
    }

    public String getAuditory() {
        return auditory;
    }

    public void setAuditory(String auditory) {
        this.auditory = auditory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Person getLector() {
        return lector;
    }

    public void setLector(Person lector) {
        this.lector = lector;
    }

    public List<Long> getGroupId() {
        return groupId;
    }

    public void setGroupId(List<Long> groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", name, auditory, lector);
    }

}
