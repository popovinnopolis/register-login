
package com.evgenijpopov.loginapp.Models;

import com.evgenijpopov.loginapp.Utils.Encryption;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class Person extends Entity implements Serializable {
    transient private static final long serialVersionUID = -1093674317281948307L;
    protected String firstName;
    protected String surnameName;
    protected String secondName;
    protected Date dateOfBirth;
    protected List<Contact> contacts;
    protected Role role;
    protected String passHash; // Hash
    private Encryption encryption = new Encryption();

    protected Person(String firstName, String surnameName, String secondName, Date dateOfBirth,
                     List<Contact> contacts, Role role, String password) {
        this();
        this.firstName = firstName;
        this.surnameName = surnameName;
        this.secondName = secondName;
        this.dateOfBirth = dateOfBirth;
        this.contacts = contacts == null ? new ArrayList<Contact>() : contacts;
        this.role = role;
        savePassword(password);
    }

    protected Person(String firstName, String surnameName, String secondName, Date dateOfBirth,
                     List<Contact> contacts, Role role) {
        this(firstName, surnameName, secondName, dateOfBirth, contacts, role, null);
    }

    protected Person(String firstName, String surnameName, String secondName, Date dateOfBirth,
                     List<Contact> contacts) {
        this(firstName, surnameName, secondName, dateOfBirth, contacts, Role.GUEST);
    }

    protected Person() {
        super();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurnameName() {
        return surnameName;
    }

    public void setSurnameName(String surnameName) {
        this.surnameName = surnameName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", firstName, surnameName, secondName);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void savePassword(String password) {
        this.passHash = encryption.hashPassword(password);
    }

    public boolean verifyPassword(String password) {
        return encryption.verifyPassword(this.passHash, password);
    }
}
