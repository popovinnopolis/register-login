package com.evgenijpopov.loginapp.Utils;

import com.evgenijpopov.loginapp.Managers.ManagerGroup;
import com.evgenijpopov.loginapp.Managers.ManagerJournal;
import com.evgenijpopov.loginapp.Managers.ManagerStudent;
import com.evgenijpopov.loginapp.Managers.ManagerUser;
import com.evgenijpopov.loginapp.Models.Contact;
import com.evgenijpopov.loginapp.Models.ContactType;
import com.evgenijpopov.loginapp.Models.Group;
import com.evgenijpopov.loginapp.Models.Journal;
import com.evgenijpopov.loginapp.Models.Lesson;
import com.evgenijpopov.loginapp.Models.Person;
import com.evgenijpopov.loginapp.Models.Role;
import com.evgenijpopov.loginapp.Models.Student;
import com.evgenijpopov.loginapp.Models.Teacher;
import com.evgenijpopov.loginapp.Models.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by evgenijpopov on 29.06.17.
 */

public class Dummie {

    static public void fill() {
//        ManagerStudent managerStudent = ManagerStudent.getInstance();
        ManagerGroup managerGroup = ManagerGroup.getInstance();
        ManagerJournal managerJournal = ManagerJournal.getInstance();
        ManagerUser managerUser = ManagerUser.getInstance();

        Group group = new Group("STC_06", null);

        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("email@email.ru", ContactType.EMAIL));
        contacts.add(new Contact("+79998887766", ContactType.PHONE));

        Person student = new Student("John", "Connor", "Reese", new Date(), group, contacts);
        Person teacher = new Teacher("Fred", "Kruger", "Charles", new Date(), null);
        managerUser.create(student);
        managerUser.create(teacher);
        managerUser.create(new User("admin", null, null, null, null, Role.ADMIN, "admin"));
        managerUser.create(new User("1", null, null, null, null, Role.ADMIN, "1"));


        Lesson lesson = new Lesson("Android-Java", new Date("2017/06/28 9:00 UTC+3"),
                new Date("2017/06/28 13:00 UTC+3"), "501", null, null, teacher, null);
        managerGroup.create(new Group("STC_07", null));
        managerGroup.create(new Group("STC_08", null));
        managerGroup.create(new Group("STC_09", null));
        managerGroup.create(new Group("STC_10", null));
        managerJournal.create(new Journal(student, lesson, true));
    }

}
