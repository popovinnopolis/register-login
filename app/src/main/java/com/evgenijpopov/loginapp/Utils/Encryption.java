package com.evgenijpopov.loginapp.Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class Encryption {
    private final String salt;
    private final String HASHING_ALGORITHM = "MD5";

    public String getSalt() {
        return salt;
    }

    public Encryption() {
        SecureRandom random = new SecureRandom();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 20; i++) {
            sb.append(String.format("%02X", random.nextInt() & 0xff));
        }
        salt = sb.toString();
    }

    public String hashPassword(String password, String salt) {
        StringBuilder hashResult = new StringBuilder(salt + "$");
        if (null == password) return null;

        password = salt + password;
        try {
            MessageDigest digest = MessageDigest.getInstance(HASHING_ALGORITHM);
            digest.update(password.getBytes(), 0, password.length());
            for (int i = 0; i < digest.getDigestLength(); i++) {
                hashResult.append(String.format("%02X", digest.digest()[i]));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        return hashResult.toString();
    }

    public String hashPassword(String password) {
        return hashPassword(password, this.salt);
    }

    public boolean verifyPassword(String hash, String password) {
        if (hash == null || password == null || (!hash.contains("$"))) return false;
        String salt = hash.substring(0, hash.indexOf('$'));
        return hash.equals(hashPassword(password, salt));
    }

}
