package com.evgenijpopov.loginapp.Utils;

import java.util.Random;

public class IdGenerator {
    public static Long getId() {
        return System.currentTimeMillis() + new Random().nextLong();
    }
}
