package com.evgenijpopov.loginapp.Utils;


import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class ReflectionUtils {
    public static Field findField(Object obj, String name) {
        for(Field field: getFields(obj)) {
            if (field.getName().equals(name)) return field;
        }
        return null;
    }

    public static ArrayList<Field> getFields(Object obj) {
        ArrayList<Field> classHierarhyFields = new ArrayList<>();
        Class cl = obj.getClass();
        while (cl != null) {
            for (Field field : cl.getDeclaredFields()) {
                if ((field.getModifiers() & Modifier.TRANSIENT) != 0) continue;
                if (((field.getModifiers() & Modifier.PRIVATE) != 0) && !cl.equals(obj.getClass())) continue;
                classHierarhyFields.add(field);
            }
            cl = cl.getSuperclass();
        }
        return classHierarhyFields;
    }


}
