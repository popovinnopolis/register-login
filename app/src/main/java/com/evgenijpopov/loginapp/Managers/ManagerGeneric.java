package com.evgenijpopov.loginapp.Managers;

import com.evgenijpopov.loginapp.Interfaces.ICrud;
import com.evgenijpopov.loginapp.Models.Entity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.evgenijpopov.loginapp.Utils.ReflectionUtils.findField;


public class ManagerGeneric<T> implements ICrud<T> {
    private List<T> entityList;

    public ManagerGeneric() {
        entityList = new ArrayList<>();
    }

    @Override
    public Long create(T item) {
        entityList.add(item);
        return ((Entity) item).getId();
    }

    @Override
    public T read(Long id) {
        if (id == null) return null;
        for (T item : entityList)
            if (((Entity) item).getId().equals(id)) return item;
        return null;
    }

    @Override
    public List<T> read() {
        return entityList;
    }

    @Override
    public Long update(T item) {
        if (delete(item)) return create(item);
        return null;
    }

    @Override
    public boolean delete(T item) {
        return entityList.remove(item);
    }

    @Override
    public boolean delete(Long id) {
        for (T item : entityList)
            if (((Entity) item).getId() == id) return delete(item);
        return false;
    }

    @Override
    public boolean delete() {
        entityList.clear();
        return true;
    }

    @Override
    public T findFirst(Map<String, Object> filter) {
        if (filter == null) return null;
        int conditionCounter;
        for (T item : entityList) {
            conditionCounter = 0;
            for (String fieldName : filter.keySet()) {
                Field field = findField(item, fieldName);
                if (field == null) break;
                if (!field.isAccessible()) field.setAccessible(true);
                try {
                    if (field.get(item).equals(filter.get(fieldName))) conditionCounter++;
                } catch (IllegalAccessException e) {
                    return null;
                }
            }
            if (conditionCounter == filter.size()) return item;
        }
        return null;
    }

    @Override
    public List<T> filter(Map<String, Object> filter) {
        if (filter == null) return entityList;
        int conditionCounter;
        List<T> listEntities = new ArrayList<>();
        for (T item : entityList) {
            conditionCounter = 0;
            for (String fieldName : filter.keySet()) {
                Field field = findField(item, fieldName);
                if (field == null) break;
                if (!field.isAccessible()) field.setAccessible(true);
                try {
                    if (field.get(item).equals(filter.get(fieldName))) conditionCounter++;
                } catch (IllegalAccessException e) {
                    return null;
                }
            }
            if (conditionCounter == filter.size()) listEntities.add(item);
        }
        return listEntities.size() > 0 ? listEntities : null;
    }

    public String toString() {
        return String.format("%s <%d>", this.getClass(), entityList.size());
    }

}
