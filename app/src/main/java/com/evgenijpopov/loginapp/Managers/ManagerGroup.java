package com.evgenijpopov.loginapp.Managers;

import com.evgenijpopov.loginapp.Models.Group;

public class ManagerGroup extends ManagerGeneric<Group> {

    private ManagerGroup() {
    }

    private static class SingletonHolder {
        private static final ManagerGroup INSTANCE = new ManagerGroup();
    }

    public static ManagerGroup getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
