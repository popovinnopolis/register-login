package com.evgenijpopov.loginapp.Managers;

import com.evgenijpopov.loginapp.Models.Group;
import com.evgenijpopov.loginapp.Models.Person;
import com.evgenijpopov.loginapp.Models.Student;


public class ManagerUser extends ManagerGeneric<Person> {

    private ManagerUser() {
    }

    private static class SingletonHolder {
        private static final ManagerUser INSTANCE = new ManagerUser();
    }

    public static ManagerUser getInstance() {
        return ManagerUser.SingletonHolder.INSTANCE;
    }

    @Override
    public Long create(Person item) {
        if (item instanceof Student) {
            Group group = ((Student)item).getGroup();
            ManagerGroup managerGroup = ManagerGroup.getInstance();
            if (managerGroup.read(group.getId()) == null) {
                managerGroup.create(group);
            }
        }
        return super.create(item);
    }
}
