package com.evgenijpopov.loginapp.Managers;

import com.evgenijpopov.loginapp.Models.Journal;

public class ManagerJournal extends ManagerGeneric<Journal> {

    private ManagerJournal() {
    }

    private static class SingletonHolder {
        private static final ManagerJournal INSTANCE = new ManagerJournal();
    }

    public static ManagerJournal getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
