package com.evgenijpopov.loginapp.Managers;

import com.evgenijpopov.loginapp.Models.Student;

public class ManagerStudent extends ManagerGeneric<Student> {

    private ManagerStudent() {
    }

    private static class SingletonHolder {
        private static final ManagerStudent INSTANCE = new ManagerStudent();
    }

    public static ManagerStudent getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
