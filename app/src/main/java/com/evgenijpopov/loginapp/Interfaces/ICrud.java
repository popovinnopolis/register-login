package com.evgenijpopov.loginapp.Interfaces;


import java.util.List;
import java.util.Map;

public interface ICrud<T> {
    Long create(T item);

    T read(Long id);

    List<T> read();

    Long update(T item);

    boolean delete(T item);

    boolean delete(Long id);

    boolean delete();

    T findFirst(Map<String, Object> filter);

    List<T> filter(Map<String, Object> filter);
}
