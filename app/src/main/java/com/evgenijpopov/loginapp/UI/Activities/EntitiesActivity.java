package com.evgenijpopov.loginapp.UI.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.evgenijpopov.loginapp.Managers.ManagerGroup;
import com.evgenijpopov.loginapp.Managers.ManagerJournal;
import com.evgenijpopov.loginapp.Managers.ManagerStudent;
import com.evgenijpopov.loginapp.Managers.ManagerUser;
import com.evgenijpopov.loginapp.Models.Contact;
import com.evgenijpopov.loginapp.Models.ContactType;
import com.evgenijpopov.loginapp.Models.Group;
import com.evgenijpopov.loginapp.Models.Journal;
import com.evgenijpopov.loginapp.Models.Lesson;
import com.evgenijpopov.loginapp.Models.Person;
import com.evgenijpopov.loginapp.Models.Student;
import com.evgenijpopov.loginapp.Models.Teacher;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Adapters.EntityAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EntitiesActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_entities);
        Intent intent = getIntent();
        boolean flag = intent.getBooleanExtra("isAdmin", false);
        Toast.makeText(this, "isAdmin: " + flag, Toast.LENGTH_SHORT).show();

        mRecyclerView = (RecyclerView) findViewById(R.id.rvEntitiesList);
        mLayoutManager = new LinearLayoutManager(this);

        List<String> entities = new ArrayList<>();
        entities.add("Groups");
        entities.add("Students");
        entities.add("Journals");

        mAdapter = new EntityAdapter(entities);


        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }
}
