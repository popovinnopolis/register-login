package com.evgenijpopov.loginapp.UI.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.evgenijpopov.loginapp.Managers.ManagerUser;
import com.evgenijpopov.loginapp.Models.Person;
import com.evgenijpopov.loginapp.Models.Role;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.Utils.Dummie;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    private Button bLoginRegister;
    private Button bLoginOk;
    private EditText etLoginLogin;
    private EditText etLoginPassword;
    private Context context;
    private String login, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Fill "database" by dummies data
        Dummie.fill();

        bLoginRegister = (Button) findViewById(R.id.bLoginRegister);
        bLoginOk = (Button) findViewById(R.id.bLoginOk);
        etLoginLogin = (EditText) findViewById(R.id.etLoginLogin);
        etLoginPassword = (EditText) findViewById(R.id.etLoginPassword);

        context = this;


        bLoginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, RegisterActivity.class), 1);
            }
        });

        bLoginOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login = etLoginLogin.getText().toString();
                Map<String, Object> condition = new HashMap<String, Object>() {{
                    put("firstName", login);
                }};
                Person user = ManagerUser.getInstance().findFirst(condition);
                if (user != null && user.verifyPassword(etLoginPassword.getText().toString())) {
                    Intent intent = new Intent(context, EntitiesActivity.class);
                    intent.putExtra("isAdmin", user.getRole().equals(Role.ADMIN));
                    startActivity(intent);
                } else {
                    Toast.makeText(context, "Error login | passHash, please try again.",
                            Toast.LENGTH_SHORT).show();
                    etLoginLogin.setText("");
                    etLoginPassword.setText("");
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            login = data.getStringExtra("login");
            password = data.getStringExtra("passHash");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        etLoginLogin.setText("");
        etLoginPassword.setText("");
    }
}
