package com.evgenijpopov.loginapp.UI.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgenijpopov.loginapp.Managers.ManagerUser;
import com.evgenijpopov.loginapp.Models.Person;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Items.UserViewHolder;

import java.util.List;
import java.util.Map;


public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {
    private List<Person> entities;

    public UserAdapter(List<Person> entities) {
        this.entities = entities;
    }

    public UserAdapter(ManagerUser managerUser, Map<String, Object> filter) {
        this.entities = managerUser.filter(filter);
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_entity, parent, false);
        return new UserViewHolder(v);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.bind(entities.get(position));
    }

    @Override
    public int getItemCount() {
        return entities.size();
    }
}

