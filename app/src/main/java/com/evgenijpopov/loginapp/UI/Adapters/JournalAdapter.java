package com.evgenijpopov.loginapp.UI.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgenijpopov.loginapp.Models.Journal;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Items.JournalViewHolder;

import java.util.List;

/**
 * Created by evgenijpopov on 29.06.17.
 */

public class JournalAdapter extends RecyclerView.Adapter<JournalViewHolder> {
    private List<Journal> entities;

    public JournalAdapter(List<Journal> entities) {
        this.entities = entities;
    }

    @Override
    public JournalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_entity, parent, false);
        return new JournalViewHolder(v);
    }

    @Override
    public void onBindViewHolder(JournalViewHolder holder, int position) {
        holder.bind(entities.get(position));
    }

    @Override
    public int getItemCount() {
        return entities.size();
    }
}

