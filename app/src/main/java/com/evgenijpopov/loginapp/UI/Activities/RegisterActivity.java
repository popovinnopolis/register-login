package com.evgenijpopov.loginapp.UI.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.evgenijpopov.loginapp.Managers.ManagerUser;
import com.evgenijpopov.loginapp.Models.Person;
import com.evgenijpopov.loginapp.Models.Role;
import com.evgenijpopov.loginapp.Models.User;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.Utils.Encryption;

public class RegisterActivity extends AppCompatActivity {
    private Context context;
    private EditText etRegisterLogin;
    private EditText etRegisterPassword1;
    private EditText etRegisterPassword2;
    private Button bRegisterOk;
    private static String login, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        context = this;
        etRegisterLogin = (EditText) findViewById(R.id.etRegisterLogin);
        etRegisterPassword1 = (EditText) findViewById(R.id.etRegisterPassword1);
        etRegisterPassword2 = (EditText) findViewById(R.id.etRegisterPassword2);
        bRegisterOk = (Button) findViewById(R.id.bRegisterOk);

        bRegisterOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etRegisterPassword1.getText().toString().equals(etRegisterPassword2.getText().toString())) {
                    Toast.makeText(context, "Error. Please enter correct passwords.", Toast.LENGTH_SHORT).show();
                    etRegisterPassword1.setText("");
                    etRegisterPassword2.setText("");
                    etRegisterPassword1.requestFocus();
                    return;
                }
                login = etRegisterLogin.getText().toString();
                password = etRegisterPassword1.getText().toString();
                User user = new User(login, null, null, null, null, Role.ADMIN, password);
                ManagerUser.getInstance().create(user);

                Intent intent = new Intent();
//                intent.putExtra("passHash", password);
//                intent.putExtra("login", login);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
