package com.evgenijpopov.loginapp.UI.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.evgenijpopov.loginapp.Managers.ManagerGroup;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Adapters.GroupAdapter;

public class GroupsActivity extends AppCompatActivity {
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);

        recyclerView = (RecyclerView) findViewById(R.id.rvGroupsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));

        // Get group list from Singleton
        GroupAdapter groupAdapter = new GroupAdapter(ManagerGroup.getInstance().read());

        recyclerView.setAdapter(groupAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
    }
}
