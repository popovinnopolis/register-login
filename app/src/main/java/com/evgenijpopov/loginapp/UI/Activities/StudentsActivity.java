package com.evgenijpopov.loginapp.UI.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.evgenijpopov.loginapp.Managers.ManagerGroup;
import com.evgenijpopov.loginapp.Managers.ManagerUser;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Adapters.GroupAdapter;
import com.evgenijpopov.loginapp.UI.Adapters.UserAdapter;

public class StudentsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students);

        recyclerView = (RecyclerView) findViewById(R.id.rvStudentsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        // Get group list from Singleton
        UserAdapter userAdapter = new UserAdapter(ManagerUser.getInstance().read());

        recyclerView.setAdapter(userAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

    }
}
