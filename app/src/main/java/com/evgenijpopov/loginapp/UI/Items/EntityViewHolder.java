package com.evgenijpopov.loginapp.UI.Items;


import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Activities.GroupsActivity;
import com.evgenijpopov.loginapp.UI.Activities.JournalsActivity;
import com.evgenijpopov.loginapp.UI.Activities.StudentsActivity;

public class EntityViewHolder extends RecyclerView.ViewHolder {
    TextView textView;

    public EntityViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item = ((TextView) v.findViewById(R.id.tvItemEntity)).getText().toString();
                Intent intent;
                switch (item) {
                    case "Students": {
                        intent = new Intent(v.getContext(), StudentsActivity.class);
                        v.getContext().startActivity(intent);
                        break;
                    }
                    case "Groups": {
                        intent = new Intent(v.getContext(), GroupsActivity.class);
                        v.getContext().startActivity(intent);
                        break;
                    }
                    case "Journals": {
                        intent = new Intent(v.getContext(), JournalsActivity.class);
                        v.getContext().startActivity(intent);
                        break;
                    }
                }
            }
        });
        textView = (TextView) itemView.findViewById(R.id.tvItemEntity);
    }

    public void bind(String s) {
        textView.setText(s);
    }
}
