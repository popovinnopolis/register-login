package com.evgenijpopov.loginapp.UI.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgenijpopov.loginapp.Models.Group;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Items.GroupViewHolder;

import java.util.List;


public class GroupAdapter extends RecyclerView.Adapter<GroupViewHolder> {
    private List<Group> groups;

    public GroupAdapter(List<Group> groups) {
        this.groups = groups;
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group_list, parent, false);
        return new GroupViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        holder.bind(groups.get(position));
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }

    public void deleteItem(int position) {
        groups.remove(position);
//        notifyDataSetChanged();
        notifyItemChanged(position);
    }
}
