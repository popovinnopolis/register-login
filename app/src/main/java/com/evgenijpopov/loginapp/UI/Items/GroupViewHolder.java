package com.evgenijpopov.loginapp.UI.Items;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.evgenijpopov.loginapp.Models.Group;
import com.evgenijpopov.loginapp.R;


public class GroupViewHolder extends RecyclerView.ViewHolder {
    private TextView textViewName, textViewCount;

    public GroupViewHolder(View itemView) {
        super(itemView);
        textViewName = (TextView) itemView.findViewById(R.id.tvGroupName);
        textViewCount = (TextView) itemView.findViewById(R.id.tvGroupCount);
    }

    public void bind(Group group) {
        textViewName.setText(group.getName());
        textViewCount.setText(group.getStudents().size()+"");
    }
}
