package com.evgenijpopov.loginapp.UI.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.evgenijpopov.loginapp.Managers.ManagerJournal;
import com.evgenijpopov.loginapp.Managers.ManagerUser;
import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Adapters.JournalAdapter;
import com.evgenijpopov.loginapp.UI.Adapters.UserAdapter;

public class JournalsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journals);

        recyclerView = (RecyclerView) findViewById(R.id.rvJournalsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        // Get group list from Singleton
        JournalAdapter journalAdapter = new JournalAdapter(ManagerJournal.getInstance().read());

        recyclerView.setAdapter(journalAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

    }
}
