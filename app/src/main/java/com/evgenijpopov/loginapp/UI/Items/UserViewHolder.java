package com.evgenijpopov.loginapp.UI.Items;


import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.evgenijpopov.loginapp.Models.Person;
import com.evgenijpopov.loginapp.R;

public class UserViewHolder extends RecyclerView.ViewHolder {
    TextView textView;

    public UserViewHolder(View itemView) {
        super(itemView);
//        itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String item = ((TextView) v.findViewById(R.id.tvItemEntity)).getText().toString();
//                Intent intent;
//                switch (item) {
//                    case "Students": {
//
//                        break;
//                    }
//                    case "Groups": {
//                        intent = new Intent(v.getContext(), GroupsActivity.class);
//                        v.getContext().startActivity(intent);
//                        break;
//                    }
//                    case "Journals": {
//
//                        break;
//                    }
//                }
//            }
//        });
        textView = (TextView) itemView.findViewById(R.id.tvItemEntity);
    }

    public void bind(Person person) {
        textView.setText(person.toString());
    }
}
