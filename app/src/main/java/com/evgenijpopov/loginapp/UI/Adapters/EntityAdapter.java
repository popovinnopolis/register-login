package com.evgenijpopov.loginapp.UI.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgenijpopov.loginapp.R;
import com.evgenijpopov.loginapp.UI.Items.EntityViewHolder;

import java.util.List;

public class EntityAdapter extends RecyclerView.Adapter<EntityViewHolder> {
    private List<String> entities;

    public EntityAdapter(List<String> entities) {
        this.entities = entities;
    }

    @Override
    public EntityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_entity, parent, false);
        return new EntityViewHolder(v);
    }

    @Override
    public void onBindViewHolder(EntityViewHolder holder, int position) {
        holder.bind(entities.get(position));
    }

    @Override
    public int getItemCount() {
        return entities.size();
    }
}
